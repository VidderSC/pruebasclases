package classes;

import java.time.LocalDate;

public class Jugador extends Persona{

    // Atributos
    private String nickname;
    private int points;

    // Constructores
    public Jugador() {
    }

    public Jugador(String name, String lastName, LocalDate dateOfBirth, String country, String nickname, int points) {
        super(name, lastName, dateOfBirth, country);
        this.nickname = nickname;
        this.points = points;
    }

    public Jugador(String name, String lastName, String nickname) {
        super(name, lastName);
        this.nickname = nickname;
    }

    public Jugador(String nickname) {
        this.nickname = nickname;
    }

    public Jugador(String name, String lastName, String nickname, int points) {
        super(name, lastName);
        this.nickname = nickname;
        this.points = points;
    }

    // Getters y Setters
    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    // toString
    @Override
    public String toString() {
        return "Jugador{" +
                "nickname='" + nickname + '\'' +
                ", points=" + points +
                '}';
    }
}
