package interfaces;

import classes.Jugador;

import java.util.List;

public interface JugadorCRUD {
    /*
      Significado de las siglas CRUD:
      Create,
      Retrieve/Read,
      Update,
      Delete,
     */

    // Create
    void save(Jugador jugador);

    // Retrieve
    List<Jugador> findAll();

    // Update
    void update(Jugador jugador);

    // Delete
    void delete(Jugador jugador);
}