package interfaces;

import classes.Persona;

import java.util.List;

public interface PersonaCRUD {
    /*
      Significado de las siglas CRUD:
      Create,
      Retrieve/Read,
      Update,
      Delete,
     */

    // Create
    void save(Persona persona);

    // Retrieve
    List<Persona> findAll();

    // Update
    void update(Persona persona);

    // Delete
    void delete(Persona persona);
}
