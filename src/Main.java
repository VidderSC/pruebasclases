import interfaces.JugadorCRUD;
import interfaces.JugadorCRUDtxt;

public class Main {
    static JugadorCRUD jugadorCRUD = new JugadorCRUDtxt();
    public static void main(String[] args) {
        int option = 0;
        do {
            System.out.println("Welcome to this program!!");
            System.out.println();
            menu();
            option = getOptions();
        } while (option != 0);

        System.out.println("Bye!");
    }

    private static void menu() {
        System.out.println("1. Enter info.");
        System.out.println("2. Save info to file.");
        System.out.println("3. Import & Show saved info on screen.");
        System.out.println("4. Search name & calculate age.");
        System.out.println("0. Exit.");
    }
    
    private static int getOptions() {
        int option = 0;
        getInfo();
        saveInfo();
        readInfo();
        printInfo();
        searchInfo();
        return option;
    }

    private static void searchInfo() {
    }

    private static void printInfo() {
    }

    private static void readInfo() {
    }

    private static void saveInfo() {
    }

    private static void getInfo() {
        getInfoPersona();
        getInfoJugador();

    }

    private static void getInfoPersona() {
    }

    private static void getInfoJugador() {
    }
}